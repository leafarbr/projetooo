package br.com.itau;

public class Funcionario extends Pessoa{

    private String racf;
    private String cargo;
    private double salario;

    public String getRacf() {
        return racf;
    }

    public void setRacf(String racf) {
        this.racf = racf;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public Funcionario(String cpf, String nome, String sexo, int idade, String racf, String cargo, double salario) {
        super(cpf, nome, sexo, idade);
        this.racf = racf;
        this.cargo = cargo;
        this.salario = salario;

    }

    @Override
    public String localizaidentificacao()
    {
        return this.getRacf();
    }
}


